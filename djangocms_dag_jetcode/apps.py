from django.apps import AppConfig


class JetcodeAppConfig(AppConfig):
    name = "djangocms_dag_jetcode"
    verbose_name = "Django CMS DAG Jetcode"
